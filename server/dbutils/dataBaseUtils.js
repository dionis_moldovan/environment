import mongoose from "mongoose";
import '../models/Data';
import '../models/DailyData';

const Data = mongoose.model('actualMeasure');
const DailyData = mongoose.model('dailyMeasure');

var MongoClient = require('mongodb').MongoClient;
var url = 'mongodb://localhost:27017/environmentDB';
var options = {
                useMongoClient: true,
                autoIndex: false,
                promiseLibrary: require('bluebird')};

export function setUpConnection() {
    mongoose.Promise = global.Promise;
    mongoose.connect(url,options);
}

export function showActual(id){
    console.log('actualMeasure results');
    Data.find({},function(err,animals){});
    return Data.find();
}

export function showDaily(id){
    console.log('dailyMeasure results');
    var startDate = new Date();
    var endDate = new Date();
    startDate.setDate(endDate.getDate() - 1);
    var query = {"time":
        {$gte: startDate,
         $lte: endDate}
        };
    var allData = DailyData.find({query, $orderby:{time:-1}}, function(err,dailyDataJson){
        var colCount = dailyDataJson.length;
        var dailyDataOverall = {};
        dailyDataOverall['CO2'] = 0;
        dailyDataOverall['CO2_mg'] = 0;
        dailyDataOverall['Formaldehyde'] = 0;
        dailyDataOverall['PM0_1'] = 0;
        dailyDataOverall['PM2_5'] = 0;
        dailyDataOverall['PM10'] = 0;
        dailyDataOverall['Pressure'] = 0;
        dailyDataOverall['Humidity'] = 0;
        dailyDataOverall['Temperature'] = 0;
        dailyDataOverall['Gas'] = 0;
        for (var i = 0; i< colCount; i++){
            dailyDataOverall['CO2'] = dailyDataOverall.CO2 + parseFloat(dailyDataJson[i].CO2);
            dailyDataOverall['CO2_mg'] = dailyDataOverall.CO2_mg + parseFloat(dailyDataJson[i].CO2_mg);
            dailyDataOverall['Formaldehyde'] = dailyDataOverall.Formaldehyde + parseFloat(dailyDataJson[i].Formaldehyde);
            dailyDataOverall['Formaldehyde_mg'] = dailyDataOverall.Formaldehyde_mg + parseFloat(dailyDataJson[i].Formaldehyde_mg);
            if (!isNaN(dailyDataJson[i].PM0_1) && dailyDataJson[i].PM0_1.indexOf('.')!=-1){
                dailyDataOverall['PM0_1'] = dailyDataOverall.PM0_1 + parseFloat(dailyDataJson[i].PM0_1);
            }
            if (!isNaN(dailyDataJson[i].PM2_5) && dailyDataJson[i].PM2_5.indexOf('.')!=-1){
                dailyDataOverall['PM2_5'] = dailyDataOverall.PM2_5 + parseFloat(dailyDataJson[i].PM2_5);
            }
            if (!isNaN(dailyDataJson[i].PM10) && dailyDataJson[i].PM10.indexOf('.')!=-1){
                dailyDataOverall['PM10'] = dailyDataOverall.PM10 + parseFloat(dailyDataJson[i].PM10);
            }
            dailyDataOverall['Pressure'] = dailyDataOverall.Pressure + parseFloat(dailyDataJson[i].Pressure);
            dailyDataOverall['Temperature'] = dailyDataOverall.Temperature + parseFloat(dailyDataJson[i].Temperature);
            dailyDataOverall['Gas'] = dailyDataOverall.Gas + parseFloat(dailyDataJson[i].Gas);

        }
        dailyDataOverall['CO2'] = (dailyDataOverall.CO2/colCount).toPrecision(3);
        dailyDataOverall['CO2_mg'] = (dailyDataOverall.CO2_mg/colCount).toPrecision(3);
        dailyDataOverall['CO2_p'] = (dailyDataOverall.CO2/10000).toPrecision(3)
        dailyDataOverall['Formaldehyde'] = dailyDataOverall.Formaldehyde/colCount;
        dailyDataOverall['Formaldehyde_p'] = dailyDataOverall.Formaldehyde/10000000;
        dailyDataOverall['Formaldehyde_mg'] = (dailyDataOverall.Formaldehyde_mg/colCount).toPrecision(3);
        dailyDataOverall['PM0_1'] = (dailyDataOverall.PM0_1/colCount).toPrecision(3);
        dailyDataOverall['PM2_5'] = (dailyDataOverall.PM2_5/colCount).toPrecision(3);
        dailyDataOverall['PM10'] = (dailyDataOverall.PM10/colCount).toPrecision(3);
        dailyDataOverall['Pressure'] = (dailyDataOverall.Pressure/colCount).toPrecision();
        dailyDataOverall['Temperature'] = (dailyDataOverall.Temperature/colCount).toPrecision(2);
        dailyDataOverall['Gas'] = (dailyDataOverall.Gas/colCount).toPrecision(3);

        //CO2
        if (dailyDataOverall.CO2_p < 0.6){
           dailyDataOverall['CO2_status'] = 'normal';
        }
        if (dailyDataOverall.CO2_p >=0.6 && dailyDataOverall.CO2_p < 1.0){
           dailyDataOverall['CO2_status'] = 'bad_air';
        }
        if (dailyDataOverall.CO2_p >=1.0 && dailyDataOverall.CO2_p < 2.5){
           dailyDataOverall['CO2_status'] = 'low_warning';
        }
        if (dailyDataOverall.CO2_p >=2.5 && dailyDataOverall.CO2_p < 5.0){
           dailyDataOverall['CO2_status'] = 'warning';
        }
        if (dailyDataOverall.CO2_p >=5.0 ){
           dailyDataOverall['CO2_status'] = 'danger';
        }
        //Formaldehyde
        if (dailyDataOverall.Formaldehyde_mg < 0.0003){
           dailyDataOverall['Formaldehyde_status'] = 'normal';
        }
        if (dailyDataOverall.Formaldehyde_mg >= 0.0003 && dailyDataOverall.Formaldehyde_mg <0.01){
           dailyDataOverall['Formaldehyde_status'] = 'warning';
        }
        if (dailyDataOverall.Formaldehyde_mg >= 0.01){
           dailyDataOverall['Formaldehyde_status'] = 'danger';
        }
        console.log(dailyDataOverall);
    });
    return allData;
}

export function showChart(id){
    console.log('show chart');
    var startDate = new Date();
    var endDate = new Date();
    var element = "CO2";
    startDate.setMonth(endDate.getMonth() - 1);
    var query = {"time":
        {$gte:startDate,
         $lte:endDate}
        }
   return DailyData.find({query,$orderby:{time:-1}} ,{[element]:1, time:1})

}
