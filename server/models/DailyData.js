import mongoose from "mongoose";

const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;

const DataSchema = new Schema({
     _id: ObjectId,
     CO2: String,
     CO2_p: String,
     CO2_mg: String,
     CO2_status: String,
     time: Date,
     Temperature: String,
     Temperature_status: String,
     Formaldehyde: String,
     Formaldehyde_p: String,
     Formaldehyde_mg: String,
     Formaldehyde_status: String,
     PM0_1: String,
     PM2_5: String,
     PM2_5_status: String,
     PM10: String,
     PM10_status: String,
     Pressure: String,
     Humidity: String,
     Gas: String

});

DataSchema.set('collection','dailyMeasure');
mongoose.model('dailyMeasure',DataSchema);
