//******************************
 //*The TX pin on the sensor connects to pin 12 on the Arduino
 //*The RX pin on the sensor connects to pin 13 on the Arduino
 //
 //*Version：V1.0
 //*Author：Moldovan Dionis
 //******************************
#include <Arduino.h>
#include <SoftwareSerial.h>
#define LENG 9   //RX Bytes
#define LENG_PM 31
unsigned char buf[LENG];
unsigned char buf_PM[LENG_PM];
byte cmd[LENG] = {0xFF,0x01,0x86,0x00,0x00,0x00,0x00,0x00,0x79}; //Send command for co2 sensor
//byte cmd[8] = {0x01,0x03,0x00,0x105,0x00,0x01,0xC1,0xF9};
uint8_t C20Concentration;
uint8_t CH20Concentration;
int PM01Value=0;          //define PM1.0 value of the air detector module
int PM2_5Value=0;         //define PM2.5 value of the air detector module
int PM10Value=0;         //define PM10 value of the air detector module
int HighLow=0;
//BME 680
#include <Wire.h>
#include <SPI.h>
#include <Adafruit_Sensor.h>
#include "Adafruit_BME680.h"

#define BME_SCK 21
#define BME_MISO 12
#define BME_MOSI 11
#define BME_CS 10

#define SEALEVELPRESSURE_HPA (1013.25)
Adafruit_BME680 bme; // I2C

SoftwareSerial C2oSerial(11, 10); // TX, RX for co2 sensor
SoftwareSerial CH20Serial(50, 51); // RX, TX for ch20 sensor
SoftwareSerial PMSerial(53,52); //RX, TX for PM sensor

void setup()
{
  C2oSerial.begin(9600);   
  C2oSerial.setTimeout(60000);    
  CH20Serial.begin(9600);
  PMSerial.begin(9600);    
  Serial.begin(9600);
  

  //BME680
  if (!bme.begin()) {
    Serial.println("Could not find a valid BME680 sensor, check wiring!");
    while (1);
  }
//   Set up oversampling and filter initialization
  bme.setTemperatureOversampling(BME680_OS_8X);
  bme.setHumidityOversampling(BME680_OS_2X);
  bme.setPressureOversampling(BME680_OS_4X);
  bme.setIIRFilterSize(BME680_FILTER_SIZE_3);
  bme.setGasHeater(320, 150); // 320*C for 150 ms
}

void loop()
{
   C2oSerial.write(cmd,LENG);
  C2oSerial.listen();
 long start = millis();
 while ((millis() - start) < 10000) {
   if (C2oSerial.available() > 0)
   {
    memset(buf,0,LENG);
    C2oSerial.readBytes(buf,LENG);
    if (checkSum(buf) == 1)
      {
      C20Concentration = (buf[2]<<8)+buf[3];
      Serial.print("CO2: ");
      Serial.print(C20Concentration);
      Serial.println(" ppm ");
      //Serial.print(float(C20Concentration/10000.0));
      //Serial.println(" %");
    }
   }
 }
 
//Formaldehyde part
CH20Serial.listen();
CH20Serial.readBytes(buf,LENG);
  if (checkSum(buf) == 1)
  {
      CH20Concentration = (buf[4]<<8) + buf[5];
  }
    Serial.print("Formaldehyde: ");
    //Serial.print(CH20Concentration);
    //Serial.println(" ppm. ");
    Serial.print(CH20Concentration/1000.0,4);
    Serial.println(" mg/m3");

 //PM part
 PMSerial.listen();
 if(PMSerial.find(0x42)){    
    PMSerial.readBytes(buf_PM,LENG_PM);
    if(buf_PM[0] == 0x4d){
      if(checkValue(buf_PM,LENG_PM)){      
        PM01Value=transmitPM01(buf_PM); //count PM1.0 value of the air detector module
        PM2_5Value=transmitPM2_5(buf_PM);//count PM2.5 value of the air detector module
        PM10Value=transmitPM10(buf_PM); //count PM10 value of the air detector module 
      }           
    } 
  }

  static unsigned long OledTimer=millis();  
    if (millis() - OledTimer >=1000) 
    {
      
      Serial.print("PM0_1: ");  
      Serial.print(PM01Value);
      Serial.println("  ug/m3");            
    
      Serial.print("PM2_5: ");  
      Serial.print(PM2_5Value);
      Serial.println("  ug/m3");     
      
      Serial.print("PM10: ");  
      Serial.print(PM10Value);
      Serial.println("  ug/m3");   
      }

//BME680 part
if (! bme.performReading()) {
    Serial.println("Failed to perform reading :(");
    return;
  }
  Serial.print("Temperature: ");
  Serial.print(bme.temperature);
  Serial.println(" *C");

  Serial.print("Pressure: ");
  Serial.print(bme.pressure / 100.0);
  Serial.println(" hPa");

  Serial.print("Humidity: ");
  Serial.print(bme.humidity);
  Serial.println(" %");

  Serial.print("Gas: ");
  Serial.print(bme.gas_resistance / 1000.0);
  Serial.println(" KOhms");

 Serial.println();
 delay(60000);
 }

unsigned char checkSum(unsigned char *bufCheck)
{
  unsigned char checksum = 0;
  for(int i=0;i<(LENG); i++){
    if (i > 0 && i< (LENG - 1)){checksum += bufCheck[i];}
  }
  checksum = ((~checksum)+1)& 255;
  if (checksum == bufCheck[8])
    {return 1;}
  else
    {return 0;}
}

char checkValue(unsigned char *thebuf, char leng)
{  
  char receiveflag=0;
  int receiveSum=0;

  for(int i=0; i<(21); i++){
  receiveSum=receiveSum+thebuf[i];
  }
  receiveSum=receiveSum + 0x42;
  if(receiveSum == ((thebuf[22])+thebuf[23]))  //check the serial data 
  {
    receiveSum = 0;
    receiveflag = 1;
  }
  
  return receiveflag;
}

int transmitPM01(unsigned char *thebuf)
{
  int PM01Val;
  PM01Val=(thebuf[10]<<8 + thebuf[11]); //count PM1.0 value of the air detector module
  return PM01Val;
}

//transmit PM Value to PC
int transmitPM2_5(unsigned char *thebuf)
{
  int PM2_5Val;
  PM2_5Val=(thebuf[12]<<8 + thebuf[13]);//count PM2.5 value of the air detector module
  return PM2_5Val;
  }

//transmit PM Value to PC
int transmitPM10(unsigned char *thebuf)
{
  int PM10Val;
  PM10Val=(thebuf[14]<<8 + thebuf[15]); //count PM10 value of the air detector module  
  return PM10Val;
}


